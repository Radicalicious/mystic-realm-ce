if not MRCE_Styles then
	rawset(_G, "MRCE_Styles", true)
	dofile("KeepShield.lua")
	dofile("revi.lua")
	dofile("SmoothSpeedTrail.lua")
	dofile("sparkles.lua")
else
	print("Styles already loaded, file not added!")
end
