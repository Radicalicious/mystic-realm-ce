/*
    Aerial Garden Hangglider

    (C) 2022 by K. "ashifolfi" J.
*/

freeslot("MT_HANGGLIDER", "S_HANGGLIDER")

mobjinfo[MT_HANGGLIDER] = {
    --$Name HangGlider
    --$Category Aerial Garden Zone
    --$Sprite UNKNA0
    doomednum = 1339,
    spawnstate = S_HANGGLIDER,
    seestate = S_HANGGLIDER,
    spawnhealth = 8,
    flags = MF_NOGRAVITY|MF_SPECIAL
}

states[S_HANGGLIDER] = {SPR_UNKN, A, -1, nil, 0, 0, S_NULL}

local function hg_touch(mo)

end
addHook("MobjThinker", hg_touch, MT_HANGGLIDER)