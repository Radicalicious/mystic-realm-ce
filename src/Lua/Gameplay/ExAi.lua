local t = {
	-- Mod Settings,
	addon = "Mystic Realm: CE", -- Change this to the name of your addon
	-- Info
	version = 1,
	subversion = 3,
	develop = false,

}

if type(ExAIVersionInfo) == "table" then
	print("ExAI already registered by previous addon: "..tostring(ExAIVersionInfo.addon))
	return
end

rawset(_G,"ExAIVersionInfo", t)

local CV_ExAI = CV_RegisterVar{
	name = 'ai_sys',
	defaultvalue = 'On',
	flags  = 0,
	PossibleValue = CV_OnOff
}
local CV_AIDebug = CV_RegisterVar{
	name = 'ai_debug',
	defaultvalue = 'Off',
	flags  = 0,
	PossibleValue = CV_OnOff
}
local CV_AISeekDist = CV_RegisterVar{
	name = 'ai_seekdist',
	defaultvalue = '512',
	flags = 0,
	PossibleValue = {MIN = 0, MAX = 9999}
}
local CV_AIAttack = CV_RegisterVar{
	name = 'ai_attack',
	defaultvalue = 'Off',
	flags = 0,
	PossibleValue = CV_OnOff
}

local newAI = function()
	return {
		jump_last = 1, // Jump history
		spin_last = 1, //Spin history
		move_last = 0, //Directional input history
		anxiety = 0, // Catch-up counter
		panic = 0, //Catch-up mode
		flymode = 0, //0 = No interaction. 1 = Grab Sonic. 2 = Sonic is latched.
		spinmode = 0, //If 1, Tails is spinning or preparing to charge spindash
		thinkfly = 0, //If 1, Tails will attempt to fly when Sonic jumps
		idlecount = 0, //Checks the amount of time without any player inputs
		bored = 0, //AI will act independently if "bored".
		drowning = 0, //AI drowning panic. 2 = Tails flies for air.
		overlay = nil, //Speech bubble overlay
		target = nil, //Enemy to target
		fight = 0, //Actively seeking/fighting an enemy
		helpmode = 0, //Used by Amy AI to hammer-shield the player
		targetnosight = 0, //How long the target has been out of view
		playernosight = 0, //How long the player has been out of view
		stalltics = 0, //Time that AI has struggled to move
		attackwait = 0, //Tics to wait before attacking again
		attackoverheat = 0 //Used by Fang to determine whether to wait
	}
end




addHook('MapLoad', function()
	for player in players.iterate do
		player.exai = newAI()
	end
end)

addHook('BotTiccmd',function(bot,cmd)
	if gamestate != GS_LEVEL then return end
	if (CV_ExAI.value == 0)
		or not(bot.botleader)
		then return false
	end
	bot.exai = $ or newAI()

	//****
	//VARS
	local aggressive = CV_AIAttack.value
	local player = bot.botleader
	local bmo = bot.mo
	local pmo = player.mo
	local pcmd = player.cmd
	local ai = bot.exai

	//Elements
	local water = 0
	if bot.mo.eflags&MFE_UNDERWATER then water = 1 end
	local flip = 1
	if (bot.mo.flags2 & MF2_OBJECTFLIP) or (bot.mo.eflags & MFE_VERTICALFLIP) then
		flip = -1
	end
	local _2d = (bot.mo.flags2 & MF2_TWOD or twodlevel)
	local scale = bot.mo.scale

	//Measurements
	local pmom = FixedHypot(pmo.momx,pmo.momy)
	local bmom = FixedHypot(bot.mo.momx,bot.mo.momy)
	local dist = R_PointToDist2(bot.mo.x,bot.mo.y,pmo.x,pmo.y)
	local zdist = FixedMul(pmo.z-bot.mo.z,scale*flip)
	local pfac = 1 //Steps ahead to predict movement
	local xpredict = bot.mo.momx*pfac+bot.mo.x
	local ypredict = bot.mo.momy*pfac+bot.mo.y
	local zpredict = bot.mo.momz*pfac+bot.mo.z
	local predictfloor = P_FloorzAtPos(xpredict,ypredict,zpredict,bot.mo.height)
	local ang = R_PointToAngle2(bot.mo.x-bot.mo.momx,bot.mo.y-bot.mo.momy,pmo.x,pmo.y)
	local followmax = 128*8*scale //Max follow distance before AI begins to enter "panic" state
	local followthres = 92*scale //Distance that AI will try to reach
	local followmin = 32*scale
	local comfortheight = 96*scale
	local touchdist = 24*scale
	local bmofloor = P_FloorzAtPos(bot.mo.x,bot.mo.y,bot.mo.z,bot.mo.height)
	local pmofloor = P_FloorzAtPos(pmo.x,pmo.y,pmo.z,pmo.height)
	local jumpheight = FixedMul(bot.jumpfactor*10,10*scale)
	local ability = bot.charability
	local ability2 = bot.charability2
	local enemydist = 0
	local enemyang = 0
	local falling = (bot.mo.momz*flip < 0)
	local predictgap = 0 //Predicts a gap which needs to be jumped
	local isjump = min(bot.pflags&(PF_JUMPED),1) //Currently jumping
	local isabil = min(bot.pflags&(PF_THOKKED|PF_GLIDING|PF_BOUNCING),1) //Currently using ability
	local isspin = min(bot.pflags&(PF_SPINNING),1) //Currently spinning
	local isdash = min(bot.pflags&(PF_STARTDASH),1) //Currently charging spindash
	local bmogrounded = (P_IsObjectOnGround(bot.mo) and not(bot.pflags&PF_BOUNCING)) // Bot ground state
	local pmogrounded = P_IsObjectOnGround(pmo) // Player ground state
	local dojump = 0 //Signals whether to input for jump
	local doabil = 0 //Signals whether to input for jump ability. Set -1 to cancel.
	local dospin = 0 //Signals whether to input for spinning
	local dodash = 0 //Signals whether to input for spindashing
	local targetfloor = nil
	local stalled = (bmom/*+abs(bmo.momz)*/ < scale and ai.move_last) //AI is having trouble catching up
	local targetdist = CV_AISeekDist.value*FRACUNIT //Distance to seek enemy targets

	//Gun cooldown for Fang
	if ai.fight == 0 then
		ai.attackoverheat = 0
		ai.attackwait = 0
	end
	if bot.panim == PA_ABILITY2 and ability2 == CA2_GUNSLINGER then
		ai.attackoverheat = $+1
		if ai.attackoverheat > 60 then
			ai.attackwait = 1
		end
	elseif ai.attackoverheat > 0 then
		ai.attackoverheat = $-1
	else ai.attackwait = 0
	end

	//Check line of sight to player
	if P_CheckSight(bmo,pmo) then ai.playernosight = 0
	else ai.playernosight = $+1
	end

	//Predict platforming
	if abs(predictfloor-bmofloor) > 24*scale
		then predictgap = 1
	end

	ai.helpmode = 0
	//Non-Tails bots are more aggressive
-- 	if ability != CA_FLY then aggressive = 1 end
	if stalled then ai.stalltics = $+1
	else ai.stalltics = 0
	end
	//Find targets
	if aggressive or ai.bored then
		searchBlockmap("objects",function (bmo,mo)
			if mo == nil then return end
			if (mo.flags&MF_BOSS or mo.flags&MF_ENEMY) and mo.health
				and P_CheckSight(bmo,mo) then
				local dist = R_PointToDist2(bmo.x,bmo.y,mo.x,mo.y)
				if (dist < targetdist)
					and (abs(mo.z - bmo.z) < FRACUNIT*280)
					and (ai.target == nil or not(ai.target.valid)
						or (ai.target.info.spawnhealth > 1)
						or R_PointToDist2(bmo.x,bmo.y,ai.target.x,ai.target.y) > dist
						)
					then
					ai.target = mo
					return true
				end
			end
		end,bmo,bmo.x-targetdist,bmo.x+targetdist,bmo.y-targetdist,bmo.y+targetdist)
-- 		searchBlockmap("objects",function(bmo,fn) print(fn.type) end, bmo)
	end
	if ai.target and ai.target.valid then
		targetfloor = P_FloorzAtPos(ai.target.x,ai.target.y,ai.target.z,ai.target.height)
	end
	//Determine whether to fight
	if ai.panic|ai.spinmode|ai.flymode //If panicking
		or (pmom >= player.runspeed)
		or not((aggressive|ai.bored|ai.fight) and ai.target and ai.target.valid) //Not ready to fight; target invalid
		or not(ai.target.flags&MF_BOSS or ai.target.flags&MF_ENEMY) //Not a boss/enemy
		or not(ai.target.health) //No health
		or (ai.target.flags2&MF2_FRET or ai.target.flags2&MF2_BOSSFLEE or ai.target.flags2&MF2_BOSSDEAD) //flashing/escape/dead state
		or (abs(targetfloor-bot.mo.z) > FixedMul(bot.jumpfactor,100*scale) and not (ability2 == CA2_GUNSLINGER)) //Unsafe to attack
		then
		ai.target = nil
		ai.fight = 0
	else
		enemydist = R_PointToDist2(bot.mo.x,bot.mo.y,ai.target.x,ai.target.y)
		if enemydist > targetdist then // Too far
			ai.target = nil
			ai.fight = 0
		elseif not P_CheckSight(bot.mo,ai.target) then //Can't see
			ai.targetnosight = $+1
			if ai.targetnosight >= 70 then
				ai.target = nil
				ai.fight = 0
			end
		else
			enemyang = R_PointToAngle2(bot.mo.x-bot.mo.momx*pfac,bot.mo.y-bot.mo.momy*pfac,ai.target.x,ai.target.y)
			ai.fight = 1
			ai.targetnosight = 0
		end
	end

	//Check water
	ai.drowning = 0
	if (water) then
		followmin = 48*scale
		followthres = 48*scale
		followmax = $/2
		if bot.powers[pw_underwater] < 35*16 then
			ai.drowning = 1
			ai.thinkfly = 0
	 		if bot.powers[pw_underwater] < 35*8 then
	 			ai.drowning = 2
	 		end
		end
	end


	//Check anxiety
	if ai.spinmode or ai.bored or ai.fight then
		ai.anxiety = 0
		ai.panic = 0
	elseif dist > followmax //Too far away
		or zdist > comfortheight //Too high/low
-- 		or stalled //Something in my way!
		then
		ai.anxiety = min($+2,70)
		if ai.anxiety >= 70 then ai.panic = 1 end
	elseif not(bot.pflags&PF_JUMPED) or dist < followmin then
		ai.anxiety = max($-1,0)
		ai.panic = 0
	end
	//Over a pit / In danger
	if bmofloor < pmofloor-comfortheight*2*flip
		and dist > followthres*2 then
		ai.panic = 1
		ai.anxiety = 70
		ai.fight = 0
	end
	//Orientation
	if (bot.pflags&PF_SPINNING or bot.pflags&PF_STARTDASH or ai.flymode == 2) then
		cmd.angleturn = pmo.angle >> 16
	elseif not(bot.climbing) and (dist > followthres or not(bot.pflags&PF_GLIDING)) then
		cmd.angleturn = ang >> 16
	end

	//Does the player need help?
	if ability2 == CA2_MELEE and not(player.powers[pw_shield]&SH_NOSTACK or player.charability2 == CA2_MELEE)
		and not(ai.spinmode|ai.anxiety|ai.panic|ai.fight) and dist < followmax then
		ai.helpmode = 1
	else
		ai.helpmode = 0
	end


	//Check boredom
	if (pcmd.buttons == 0 and pcmd.forwardmove == 0 and pcmd.sidemove == 0)
		and not(ai.drowning|ai.panic|ai.fight|ai.helpmode)
		and bmogrounded
		then
		ai.idlecount = $+1
	else
		ai.idlecount = 0
	end
	if ai.idlecount > 35*8 or (aggressive and ai.idlecount > 35*3) then
		ai.bored = 1
	else
		ai.bored = 0
	end

	//********
	//HELP MODE
	if ai.helpmode then
		cmd.forwardmove = 25
		cmd.angleturn = ang >> 16
		if dist < scale*64 then
			dospin = 1
			dodash = 1
		end
	end

	//********
	//FLY MODE
	if ability == CA_FLY then
		//Update carry state
		if ai.flymode then
			bot.pflags = $ | PF_CANCARRY
		else
			bot.pflags = $ & ~PF_CANCARRY
		end
		//spinmode check
		if ai.spinmode == 1 then ai.thinkfly = 0
		else
			//Activate co-op flight
			if ai.thinkfly == 1 and player.pflags&PF_JUMPED then
				dojump = 1
				doabil = 1
				ai.flymode = 1
				ai.thinkfly = 0
			end
			//Check positioning
			//Thinker for co-op fly
			if not(ai.bored) and not(ai.drowning) and dist < touchdist and P_IsObjectOnGround(pmo) and P_IsObjectOnGround(bot.mo)
				and not(player.pflags&PF_STASIS)
				and pcmd.forwardmove == 0 and pcmd.sidemove == 0
				and player.dashspeed == 0
				and pmom == 0 and bmom == 0
				then
				ai.thinkfly = 1
			else ai.thinkfly = 0
			end
			//Set carried state
			if pmo.tracer == bot.mo
				and player.powers[pw_carry]
				then
				ai.flymode = 2
			end
			//Ready for takeoff
			if ai.flymode == 1 then
				ai.thinkfly = 0
				if zdist < -64*scale or bot.mo.momz*flip > scale then // Make sure we're not too high up
					doabil = -1
				else
					doabil = 1
				end
				//Abort if player moves away or spins
				if dist > followthres or player.dashspeed > 0 then
					ai.flymode = 0
				end
			//Carrying; Read player inputs
			elseif ai.flymode == 2 then
				cmd.forwardmove = pcmd.forwardmove
				cmd.sidemove = pcmd.sidemove
				if pcmd.buttons&BT_USE then
					doabil = -1
				else
					doabil = 1
				end
				//End flymode
				if not(player.powers[pw_carry])
					then
					ai.flymode = 0
				end
			end
		end
		if ai.flymode > 0 and bmogrounded and not(pcmd.buttons&BT_JUMP)
		then ai.flymode = 0 end
	else
		ai.flymode = 0
		ai.thinkfly = 0
	end

	//********
	//SPINNING
	if ability2 == CA2_SPINDASH then
		if (ai.panic or ai.flymode or ai.fight) or not(player.pflags&PF_SPINNING) or player.pflags&PF_JUMPED then ai.spinmode = 0
		else
			if not(_2d) then
			//Spindash
				if (player.dashspeed) then
					if dist < followthres and dist > touchdist then //Do positioning
						cmd.angleturn = ang >> 16
						cmd.forwardmove = 50
						ai.spinmode = 1
					elseif dist < touchdist then
						cmd.angleturn = pmo.angle >> 16
						dodash = 1
						ai.spinmode = 1
					else ai.spinmode = 0
					end
				//Spin
				elseif (player.pflags&PF_SPINNING and not(player.pflags&PF_STARTDASH)) then
					dospin = 1
					dodash = 0
					cmd.angleturn = ang >> 16
					cmd.forwardmove = 50
					ai.spinmode = 1
				else ai.spinmode = 0
				end
			//2D mode
			else
				if ((player.dashspeed and bmom == 0) or (player.dashspeed == bot.dashspeed and player.pflags&PF_SPINNING))
					then
					dospin = 1
					dodash = 1
					ai.spinmode = 1
				end
			end
		end
	else
		ai.spinmode = 0
	end
	//******
	//FOLLOW
	if not(ai.flymode or ai.spinmode or ai.fight or ai.helpmode or bot.climbing) then
		//Bored
		if ai.bored then
			local b1 = 256|128|64
			local b2 = 128|64
			local b3 = 64
			if ai.idlecount&b1 == b1 then
				cmd.forwardmove = 35
				cmd.angleturn = (ang + ANGLE_270) >> 16
			elseif ai.idlecount&b2 == b2 then
				cmd.forwardmove = 25
				cmd.angleturn = (ang + ANGLE_67h) >> 16
			elseif ai.idlecount&b3 == b3 then
				cmd.forwardmove = 15
				cmd.angleturn = (ang + ANGLE_337h) >> 16
			else
				cmd.angleturn = (ai.idlecount*(ANG1/2)) >> 16
			end
		//Too far
		elseif ai.panic or dist > followthres then
			if not(_2d) then cmd.forwardmove = 50
			elseif pmo.x > bot.mo.x then cmd.sidemove = 50
			else cmd.sidemove = -50 end
		//Within threshold
		elseif not(ai.panic) and dist > followmin and abs(zdist) < 192*scale then
			if not(_2d) then cmd.forwardmove = FixedHypot(pcmd.forwardmove,pcmd.sidemove)
			else cmd.sidemove = pcmd.sidemove end
		//Below min
		elseif dist < followmin then
			if not(ai.drowning) then
				//Copy inputs
				cmd.angleturn = pmo.angle >> 16
				bot.drawangle = ang
				cmd.forwardmove = pcmd.forwardmove*8/10
				cmd.sidemove = pcmd.sidemove*8/10
			else //Water panic?
				cmd.angleturn = (ang+ANGLE_45) >> 16
				cmd.forwardmove = 50
			end
		end
	end

	//*********
	//JUMP
	if not(ai.flymode|ai.spinmode|ai.fight) then

		//Flying catch-up code
		if isabil and ability == CA_FLY then
			cmd.forwardmove = min(50,dist/scale/8)
			if zdist < -64*scale and(ai.drowning!=2) then doabil = -1
			elseif zdist > 0 then
				doabil = 1
				dojump = 1
			end
		end


		//Start jump
		if (
			(zdist > 32*scale and player.pflags & PF_JUMPED) //Following
			or (zdist > 64*scale and ai.panic) //Vertical catch-up
			or (ai.stalltics > 25
				and (not bot.powers[pw_carry])) //Not in carry state
			or(isspin) //Spinning
			) then
			dojump = 1
-- 			print("start jump")

		//Hold jump
		elseif isjump and (zdist > 0 or ai.panic) then
			dojump = 1
-- 			print("hold jump")
		end

		//********
		//ABILITIES
		if not(ai.fight) then
			//Thok
			if ability == CA_THOK and (ai.panic or dist > followmax)
				then
				dojump = 1
				doabil = 1
			//Fly
			elseif ability == CA_FLY and (ai.drowning == 2 or ai.panic)
				then
				dojump = 1
				doabil = 1
			//Glide and climb / Float
			elseif (ability == CA_GLIDEANDCLIMB or ability == CA_FLOAT)
				and (
					(zdist > 16*scale and dist > followthres)
					or (
						(ai.panic //Panic behavior
							and (bmofloor*flip < pmofloor or dist > followmax or ai.playernosight)
						)
						or (isabil //Using ability
							and (
								(abs(zdist) > 0 and dist > followmax) //Far away
								or (zdist > 0) //Below player
							)
							and not(bmogrounded)
						)
					)
				)
				then
				dojump = 1
				doabil = 1
				if (dist < followmin and ability == CA_GLIDEANDCLIMB) then
					cmd.angleturn = pmo.angle >> 16 //Match up angles for better wall linking
				end
			//Pogo Bounce
			elseif (ability == CA_BOUNCE)
				and (
					(ai.panic and (bmofloor*flip < pmofloor or dist > followthres))
					or (isabil //Using ability
						and (
							(abs(zdist) > 0 and dist > followmax) //Far away
							or (zdist > 0) //Below player
						)
						and not(bmogrounded)
					)
				)
				then
				dojump = 1
				doabil = 1
			end
		end
	end
	//Climb controls
	if bot.climbing then
		if not(ai.fight) and zdist > 0
			then cmd.forwardmove = 50
		end
		if (ai.stalltics > 30)
			then doabil = -1
		end
	end

	if ai.anxiety and ai.playernosight > 64 then
		if leveltime&(64|32) == 64|32 then
			cmd.sidemove = 50
		elseif leveltime&32 then
			cmd.sidemove = -50
		end
	end

	//*******
	//FIGHT
	if ai.fight then
		cmd.angleturn = enemyang >> 16
		local dist = 128*scale //Distance to catch up to.
		local mindist = 64*scale //Distance to attack from. Gunslingers avoid getting this close
		local attkey = BT_JUMP
		local attack = 0
		//Standard fight behavior
		if ability2 == CA2_GUNSLINGER then //Gunslingers shoot from a distance
			mindist = abs(ai.target.z-bot.mo.z)*3/2
			dist = max($+mindist,512*scale)
			attkey = BT_USE
		elseif ability2 == CA2_MELEE then
			mindist = 96*scale
			attkey = BT_USE
		elseif bot.charflags&SF_NOJUMPDAMAGE then
			mindist = 128*scale
			attkey = BT_USE
		else //Jump attack should be timed relative to movespeed
			mindist = bmom*10+ 24*scale
		end

		if enemydist < mindist then //We're close now
			if ability2 == CA2_GUNSLINGER then //Can't shoot too close
				cmd.forwardmove = -50
			else
				attack = 1
				cmd.forwardmove = 20
			end
		elseif enemydist > dist then //Too far
			cmd.forwardmove = 50
		else //Midrange
			if ability2 == CA2_GUNSLINGER then
				if not(ai.attackwait) then
				attack = 1
				//Make Fang find another angle after shots
				else
					dojump = 1
					if predictfloor-bmofloor > -32*scale then
-- 						bmo.angle = leveltime*FRACUNIT
						if leveltime&128 then cmd.sidemove = 30
						else cmd.sidemove = -30
						end
					end
				end
			else
				cmd.forwardmove = 30 //Moderate speed so we don't overshoot the target
			end
		end
		//Attack
		if attack then
			if (attkey == BT_JUMP and (ai.target.z-bot.mo.z)*flip >= 0)
				then dojump = 1
			elseif (attkey == BT_USE)
				then
				dospin = 1
				dodash = 1
			end
		end
		//Platforming during combat
		if (ability2 != CA2_GUNSLINGER and enemydist < followthres and ai.target.z > bot.mo.z+32*scale) //Target above us
				or (ai.stalltics > 25) //Stalled
				or (predictgap)//Jumping a gap
			then
			dojump = 1
		end
	end

	//**********
	//DO INPUTS
	//Jump action
	if (dojump) then
		if ((isjump and ai.jump_last and not falling) //Already jumping
				or (bmogrounded and not ai.jump_last)) //Not jumping yet
			and not(isabil or bot.climbing) //Not using abilities
			then cmd.buttons = $|BT_JUMP
		elseif bot.climbing then //Climb up to position
			cmd.forwardmove = 50
		end
	end
	//Ability
	if (doabil == 1) then
		if ((isjump and not ai.jump_last) //Jump, released input
				or (isabil)) //Already using ability
			and not(bot.climbing) //Not climbing
			and not(ability == CA_FLY and ai.jump_last) //Flight input check
			then cmd.buttons = $|BT_JUMP
		elseif bot.climbing then //Climb up to position
			cmd.forwardmove = 50
		end
	//"Force cancel" ability
	elseif (doabil == -1)
		and ((ability == CA_FLY and isabil) //If flying, descend
			or bot.climbing) //If climbing, let go
		then
		if not ai.spin_last then
			cmd.buttons = $|BT_USE
		end
		cmd.buttons = $&~BT_JUMP
	end

	//Spin while moving
	if (dospin) then
		if (not(bmogrounded) //For air hammers
				or (abs(cmd.forwardmove)+abs(cmd.sidemove) > 0 and (bmom > scale*5 or ability2 == CA2_MELEE))) //Don't spin while stationary
			and not(ai.spin_last)
			then cmd.buttons = $|BT_USE
		end
	end
	//Charge spindash (spin from standstill)
	if (dodash) then
		if (not(bmogrounded) //Air hammers
				or (bmom < scale and not ai.spin_last) //Spin only from standstill
				or (isdash) //Already spinning
			) then
			cmd.buttons = $|BT_USE
		end
	end

	//*******
	//History
	if (cmd.buttons&BT_JUMP) then
		ai.jump_last = 1
	else
		ai.jump_last = 0
	end
	if (cmd.buttons&BT_USE) then
		ai.spin_last = 1
	else
		ai.spin_last = 0
	end
	if FixedHypot(cmd.forwardmove,cmd.sidemove) >= 30 then
		ai.move_last = 1
	else
		ai.move_last = 0
	end

	//*******
	//Aesthetic
	//thinkfly overlay
	if ai.overlay == nil or ai.overlay.valid == false then
		ai.overlay = P_SpawnMobj(bot.mo.x, bot.mo.y, bot.mo.z, MT_OVERLAY)
		ai.overlay.target = bot.mo
	end
	if ai.thinkfly == 1 then
		if ai.overlay.state == S_NULL then
			ai.overlay.state = S_FLIGHTINDICATOR
		end
	else ai.overlay.state = S_NULL
	end

	if ((mapheaderinfo[gamemap].lvlttl == "Dimension Warp") or (mapheaderinfo[gamemap].lvlttl == "Primordial Abyss")) then
		if not bot.valid then return end
		bmo.momx = 0
		bmo.momy = 0
		bmo.momz = 0
		bmo.reactiontime = -1
		bmo.player.powers[pw_ignorelatch] = 32768
		bmo.flags = $|MF_NOCLIP|MF_NOCLIPTHING|MF_SCENERY|MF_NOTHINK
		bmo.flags2 = $|MF2_DONTDRAW
		bmo.state = S_INVISIBLE
	end

	//Debug
	if CV_AIDebug.value == 1 then
		local p = "follow"
		if ai.flymode == 1 then p = "flymode (ready)"
		elseif ai.flymode == 2 then p = "flymode (carrying)"
		elseif ai.helpmode then p = "helpmode"
		elseif ai.targetnosight then p = "\x82 targetnosight " + ai.targetnosight
		elseif ai.fight then p = "fight"
		elseif ai.drowning then p = "\x85 drowning"
		elseif ai.panic then p = "\x85 panic (anxiety " + ai.anxiety + ")"
		elseif ai.bored then p = "bored"
		elseif ai.thinkfly then p = "thinkfly"
		elseif ai.anxiety then p = "\x82 anxiety " + ai.anxiety
		elseif ai.playernosight then p = "\x82 playernosight " + ai.playernosight
		elseif ai.spinmode then p = "spinmode (dashspeed " + bot.dashspeed/FRACUNIT+")"
		elseif dist > followthres then p = "follow (far)"
		elseif dist < followmin then p = "follow (close)"
		end
		local dcol = ""
		if dist > followmax then dcol = "\x85" end
		local zcol = ""
		if zdist > comfortheight then zcol = "\x85" end
		//AI States
		print("AI "..#bot.." ["+ai.bored..ai.helpmode..ai.fight..ai.attackwait..ai.thinkfly..ai.flymode..ai.spinmode..ai.drowning..ai.anxiety..ai.panic+"] "+ p)
		//Distance
		print(dcol + "dist " + dist/scale +"/"+ followmax/scale + "  " + zcol + "zdist " + zdist/scale +"/"+ comfortheight/scale)
		//Physics and Action states
		print("perf " + isjump..isabil..isspin..isdash + "|" + dojump..doabil..dospin..dodash + "  gap " + predictgap + "  stall " + ai.stalltics)
		//Inputs
		print("FM "+cmd.forwardmove + "  SM " + cmd.sidemove+"  Jmp "+(cmd.buttons&BT_JUMP)/BT_JUMP+"  Spn "+(cmd.buttons&BT_USE)/BT_USE+ "  Th "+(bot.pflags&PF_THOKKED)/PF_THOKKED)
	end

	return true
end)



print("\x81 ExAI - Version 1.0 - Released 2019/12/27",
"\x81 Enable/disable via ai_sys in console.",
"\x81 Use ai_attack and ai_seekdist to control AI aggressiveness.",
"\x81 Enable ai_debug to stream local variables and cmd inputs.")