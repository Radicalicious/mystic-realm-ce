--=======--
freeslot(
"MT_URING"
)
sfxinfo[freeslot("sfx_marioe")].caption = "Correct Solution"
local debug = 0

--despawn ring maces in ultimate mode

addHook("MobjSpawn", function(mobj)
    if not mobj and mobj.valid then return end

    if not ultimatemode then return end

	mobj.state = S_NULL

end, MT_RING)

addHook("MobjSpawn", function(mobj)
    if not mobj and mobj.valid then return end

    if not ultimatemode then return end

	mo.state = S_NULL

end, MT_COIN)

//credit to katsy for this tiny function pulled from reveries. it's so simple I could've written it myself, but eh.
--Makes elemental shield not render its fire when in water. Because fire can't burn in water. Obviously.

mobjinfo[MT_ELEMENTAL_ORB].spawnstate = S_ELEM1
mobjinfo[MT_ELEMENTAL_ORB].seestate = S_ELEMF1

addHook("MobjThinker", function(shield)
	if (shield.target) and shield.target.valid then
		if (shield.target.type == MT_ELEMENTAL_ORB) then
			if ((shield.target.target.eflags & MFE_UNDERWATER or shield.target.target.eflags & MFE_TOUCHWATER) and not (shield.target.target.eflags & MFE_TOUCHLAVA)) then
				shield.flags2 = $|MF2_DONTDRAW
			elseif (shield.flags2 & MF2_DONTDRAW) then
				shield.flags2 = $ & ~MF2_DONTDRAW
			end
		end
	end
end, MT_OVERLAY)

--air bubble thinker
addHook("TouchSpecial", function(mo, toucher)
	if toucher.player
	and MRCE_isHyper(toucher.player) then
		return true -- hyper forms don't need bubbles
	elseif (toucher.player.yusonictable and toucher.player.yusonictable.hypersonic and toucher.player.mo.skin == "adventuresonic") then return true
	else toucher.player.powers[pw_spacetime] = 12 * TICRATE end --midnight freeze's ice water uses space countdown
end, MT_EXTRALARGEBUBBLE)

addHook("MobjSpawn", function(mobj)
	if netgame then
		mobj.fuse = 30
	else
		mobj.fuse = 70
	end
end, MT_CYBRAKDEMON_FLAMEREST)

addHook("MobjFuse", function(mobj)
	P_RemoveMobj(mobj)
end, MT_CYBRAKDEMON_FLAMEREST)
