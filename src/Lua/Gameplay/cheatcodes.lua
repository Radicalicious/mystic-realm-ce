//cheat codes. they're mostly here for debug purposes, but they're fun to mess with, so I'll likely be keeping them here.
//currently only 2 cheats are present; hyper cheat, which gives all 7 emeralds and unlocks hyper form, 
//and fly cheat, which allows super sonic to fly outside of dwz
//update, glow aura adds blendmode aura to sonic's rebound dash , jump ball, spindash, and roll. Was maybe thinking as a 100% complete reward, until then, it's an easter egg

COM_AddCommand("mrsecret", function(player, arg1, arg2)
	if (gamestate == GS_LEVEL) and player.valid then
		if arg1 and arg1 == "4126" then
			if not netgame then
				player.mrce.hypercheat = true
				if modifiedgame == false then
					COM_BufInsertText(player, "devmode 1")
					COM_BufInsertText(player, "devmode 0")
				end
			else
				print(player.name .. " is a sussy baka")
				P_DamageMobj(player.mo,nil,nil,1,DMG_INSTAKILL)
			end
		elseif arg1 == "20071101" then
			player.mrce.flycheat = true
		elseif arg1 and arg1 == "radical" then
			if not player.mrce.snowboard then
				player.mrce.snowboard = true
			else
				player.mrce.snowboard = false
			end
		elseif arg1 and arg1 == "697842" then
			if not netgame 
			or ((player == server) or IsPlayerAdmin(player)) then
				if mrce_hyperstones > 0 and arg2 and arg2 == "-reset" then
					mrce_hyperstones = 0
					CONS_Printf(player, "\131Elemental Key Shards have been returned to their resting places in the temples")
				elseif mrce_hyperstones == 0 and arg2 and arg2 == "-reset" then
					CONS_Printf(player, "\133You don't have any doofus")
				elseif mrce_hyperstones != 7 then
					if modifiedgame == false then
						COM_BufInsertText(player, "devmode 1")
						COM_BufInsertText(player, "devmode 0")
					end
					mrce_hyperstones = 7
					CONS_Printf(player, "\131Received all 3 Elemental Key Shards")
				else
					CONS_Printf(player, "\133You already have all 3. Try adding '-reset' if you want to get rid of them")
				end
			end
		elseif arg1 and (arg1 == "0" or arg1 == "off") then
			player.mrce.flycheat = false
			player.mrce.hypercheat = false
			player.mrce.exspark = false
			player.mrce.glowaura = 0
			if player.pet then
				player.mo.pet.blendmode = 0
				player.mo.pet.colorized = false
			end
			if player.followmobj then
				player.followmobj.blendmode = 0
				player.followmobj.colorized = false
			end
			if player.buddies then
				for id,buddy in pairs(player.buddies) do
					if buddy.mo and buddy.mo.valid then
						buddy.mo.blendmode = 0
						buddy.mo.colorized = false
					end
				end
			end
		elseif arg1 == "20100523" then
			if player.mrce.exspark then
				player.mrce.exspark = false
			else
				player.mrce.exspark = true
				if arg2 and R_GetColorByName(arg2) then
					player.mrce.exsparkcolor = R_GetColorByName(arg2)
				else
					player.mrce.exsparkcolor = player.mo.color
				end
			end
		elseif player.mrce.exspark == true and arg1 and (arg1 == "sparkcolor" or arg1 == "66279") and arg2 and R_GetColorByName(arg2) then
			player.mrce.exsparkcolor = R_GetColorByName(arg2)
		elseif arg1 == "1207" or arg1 == "glow" then
			if player.mrce.glowaura > 0 and not arg2 then
				player.mrce.glowaura = 0
				player.mo.colorized = false
				player.mo.blendmode = 0
				if player.pet then
					player.mo.pet.blendmode = 0
					player.mo.pet.colorized = false
				end
				if player.followmobj then
					player.followmobj.blendmode = 0
					player.followmobj.colorized = false
				end
				if player.buddies then
					for id,buddy in pairs(player.buddies) do
						if buddy.mo and buddy.mo.valid then
							buddy.mo.blendmode = 0
							buddy.mo.colorized = false
						end
					end
				end
			else
				if player.spinitem == MT_THOK then
					player.spinitem = 0
				end
				if not arg2
				or arg2 == "1" or arg2 == "add" then
					player.mrce.glowaura = 1
					if player.pet then
						player.mo.pet.blendmode = AST_ADD
						if not (player.mo.pet.color == SKINCOLOR_NONE) then
							player.mo.pet.colorized = true
						end
					end
				elseif arg2 and (arg2 == "2" or arg2 == "subtract") then
					player.mrce.glowaura = 2
					if player.pet then
						player.mo.pet.blendmode = AST_SUBTRACT
						if not (player.mo.pet.color == SKINCOLOR_NONE) then
							player.mo.pet.colorized = true
						end
					end
				end
			end
		else
			CONS_Printf(player, "\133invalid input")
		end
	else
		CONS_Printf(player, "\133You must be in a level to use this")
	end
end, 0)
